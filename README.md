# Quill Example

Very simple project containing just a few classes with code snippet taken from [Quill](https://getquill.io/)'s
documentations.

Initial project structure retrieved from [Scatie](https://scastie.scala-lang.org/)'s 
[QwOewNEiR3mFlKIM7v900A](https://scastie.scala-lang.org/QwOewNEiR3mFlKIM7v900A).
