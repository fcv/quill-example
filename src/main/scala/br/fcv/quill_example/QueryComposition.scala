package br.fcv.quill_example

import io.getquill.{EntityQuery, PostgresDialect, Quoted, SnakeCase, SqlMirrorContext}

object QueryComposition {

  val ctx = new SqlMirrorContext(PostgresDialect, SnakeCase)
  import ctx._

  case class Circle(radius: Float)

  val pi: Quoted[Double] = quote(3.14159)

  val area: Quoted[Circle => Double] = quote {
    (c: Circle) => {
      val r2 = c.radius * c.radius
      pi * r2
    }
  }

  def main(args: Array[String]): Unit = {

    val quotedQuery: Quoted[EntityQuery[Circle]] = quote {
      query[Circle].filter { c =>
        val a = area(c)
        a > 10 && a < 100
      }
    }

    val qm: QueryMirror[Circle] = ctx.run(quotedQuery)
    // SELECT c.radius FROM circle c WHERE (3.14159 * (c.radius * c.radius)) > 10 AND (3.14159 * (c.radius * c.radius)) < 100
    println(qm.string)
    // Note that even though `area` is only called once and its result is "cache" as a local value within `filter`
    // method the area SQL expression is still rendered twice, thus referential transparency seems key here..
    // caching results from a function like `random` might lead to unexpected results
  }
}
