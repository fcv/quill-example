package br.fcv.quill_example

import io.getquill._

/**
 * Based on https://scastie.scala-lang.org/QwOewNEiR3mFlKIM7v900A
 */
object Main {

  val ctx = new SqlMirrorContext(PostgresDialect, SnakeCase)
  import ctx._

  case class Person(name: String, age: Int)

  def main(args: Array[String]): Unit = {

    val qm: QueryMirror[Person] = ctx.run(query[Person].filter(p => p.name == "John" && p.age < 12))

    // SELECT p.name, p.age, p.birthdate FROM person p WHERE p.name = 'John' AND p.age < 12
    println(qm.string)
  }

  def parameterizedQuery(name: String, age: Int): QueryMirror[Person] = {
    val q = ctx.run(query[Person].filter(p => p.name.like(lift(name)) && p.age < lift(age)))
    // SELECT p.name, p.age FROM person p WHERE p.name like ? AND p.age < ?
    println(q.string)
    q
  }
}
